#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>

int main ()
{
	sf::RenderWindow window(sf::VideoMode(800,600), "Business Simulator", sf::Style::Default);
	window.setVerticalSyncEnabled(true);
	sf::Clock clock;

		sf::Event event;
		while (window.pollEvent(event))

		{
			switch (event.type)
			{
				case sf::Event::Closed:
				  window.close();
					break;

				case sf::Event::KeyPressed:
					if (event.key.code == sf::Keyboard::Escape)
    				{
    					window.close();
    					break;
    				}
				window.clear(sf::Color::White);

				default:
					break;
			}
		}
	}

	return 0;
}
